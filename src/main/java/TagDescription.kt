import edu.wpi.first.apriltag.AprilTagDetection
import edu.wpi.first.cameraserver.CameraServer
import edu.wpi.first.math.geometry.Transform3d
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import kotlin.math.round


/*
all code to add info to 2nd stream is here to help keep things neat
since its all just big ugly functions that would be like 90% of the
lines in the DetectionPipeline otherwise
 */


val COLOR = Scalar(100.0, 250.0, 100.0)
val outputStream = CameraServer.putVideo("rectangle", 1280, 720)

inline fun tagDescription(mat: Mat?, detection: AprilTagDetection, transform: Transform3d, theta: Double) {
    drawEdges(mat, detection.corners)

    // rounded values
    val rx = round(transform.x * 100) / 100
    val ry = round(transform.y * 100) / 100
    val rz = round(transform.z * 100) / 100

    // id
    Imgproc.putText(
            mat,
            detection.id.toString(),
            Point(
                    detection.getCornerX(0) + 20,
                    detection.getCornerY(0) + 20
            ),
            0,
            0.7,
            COLOR,
            2
    )
    Imgproc.putText(
            mat,
            (round(theta*1000)/1000).toString(),
            Point(
                    detection.getCornerX(0) + 20,
                    detection.getCornerY(0) + 40
            ),
            0,
            0.7,
            COLOR,
            2
    )
    Imgproc.putText(
            mat,
            "$rx, $ry, $rz",
            Point(
                    detection.getCornerX(0) + 20,
                    detection.getCornerY(0) + 60
            ),
            0,
            0.7,
            COLOR,
            2
    )
}

inline fun drawEdges(mat: Mat?, corners: DoubleArray) {

    // this could 100% just be a loop
    Imgproc.line(
            mat,
            Point(
                    corners[0],
                    corners[1]
            ),
            Point(
                   corners[2],
                    corners[3]
            ), COLOR, 3)
    Imgproc.line(
            mat,
            Point(
                    corners[2],
                    corners[3]
            ),
            Point(
                    corners[4],
                    corners[5]
            ), COLOR, 3)
    Imgproc.line(
            mat,
            Point(
                    corners[4],
                    corners[5]
            ),
            Point(
                    corners[6],
                    corners[7]
            ), COLOR, 3)
    Imgproc.line(
            mat,
            Point(
                    corners[6],
                    corners[7]
            ),
            Point(
                    corners[0],
                    corners[1]
            ), COLOR, 3)
}