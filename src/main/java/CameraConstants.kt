



const val FOCAL_LENGTH_X = 575.52363212
const val FOCAL_LENGTH_Y = 571.85951839
const val FOCAL_CENTER_X = 610.39514844
const val FOCAL_CENTER_Y = 336.97503347


/*

 Camera matrix:
[[575.52363212   0.         610.39514844]
 [  0.         571.85951839 336.97503347]
 [  0.           0.           1.        ]]

 Distortion coefficient:
[[-0.42383315  0.44350146  0.00617732  0.00089983 -0.32325311]]

 */