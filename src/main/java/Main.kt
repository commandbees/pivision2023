import edu.wpi.first.apriltag.AprilTagDetector
import edu.wpi.first.apriltag.AprilTagPoseEstimator
import edu.wpi.first.cameraserver.CameraServer
import edu.wpi.first.cscore.VideoSource
import edu.wpi.first.networktables.NetworkTableInstance
import edu.wpi.first.vision.VisionPipeline
import edu.wpi.first.vision.VisionThread
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc
import kotlin.math.atan

const val teamNumber: Int = 8060
val camera: VideoSource = CameraServer.startAutomaticCapture()

fun main(args: Array<String>) {
    camera.setResolution(1280, 720)
    camera.setFPS(15)

    val visionThread = VisionThread(camera, DetectionPipeline()) {
        // do stuff with outputs here

    }
    visionThread.start()

    // loop forever
    for (i in generateSequence(0) { it }) {
        Thread.sleep(1000)
    }
}

class DetectionPipeline : VisionPipeline {
    private var frame = 0
    private val table = NetworkTableInstance.getDefault().getTable("visionTable")
    private val detector = buildAprilTagDetector()
    private val poseEstimator = AprilTagPoseEstimator(AprilTagPoseEstimator.Config(
            0.1524,
            FOCAL_LENGTH_X,
            FOCAL_LENGTH_Y,
            FOCAL_CENTER_X,
            FOCAL_CENTER_Y
    ))
    private val publishers = Array(8) { i -> table.getDoubleArrayTopic("tag$i").publish()}
    private val outputStream = CameraServer.putVideo("rectangle", 1280, 720)

    private inline fun buildAprilTagDetector(): AprilTagDetector {
        val detector = AprilTagDetector()
        detector.addFamily("tag16h5", 0)
        detector.config.quadSigma = 0.8f
        detector.quadThresholdParameters.minClusterPixels = 250
        detector.quadThresholdParameters.criticalAngle = 50.0
        detector.quadThresholdParameters.maxLineFitMSE = 15f
        return detector
    }

    override fun process(mat: Mat?) {
        frame += 1
        val grayMat = Mat()
        Imgproc.cvtColor(mat, grayMat, Imgproc.COLOR_RGB2GRAY)
        val detections = detector.detect(grayMat)
        for (detection in detections) {
            if (detection.id > 8 || detection.id < 1)
                continue
            val transformation = poseEstimator.estimate(detection)

            // arctan is precomputed here as its expensive and used in a couple places
            val arcTan = atan(transformation.x/transformation.z)

            tagDescription(mat, detection, transformation, arcTan)
            publishers[detection.id - 1].set(doubleArrayOf(
                    transformation.x,
                    transformation.y,
                    transformation.z,
                    arcTan,

                    // probably not needed
                    transformation.rotation.quaternion.x,
                    transformation.rotation.quaternion.y,
                    transformation.rotation.quaternion.z,
                    transformation.rotation.quaternion.w
            ))
        }
        outputStream.putFrame(mat)
        grayMat.release()
    }
}